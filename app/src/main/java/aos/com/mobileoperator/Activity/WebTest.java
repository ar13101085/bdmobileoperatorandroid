package aos.com.mobileoperator.Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import aos.com.mobileoperator.R;

public class WebTest extends AppCompatActivity {

    WebView webView;
    String contactNumber = "";
    boolean isFinish=false;
    String address = "file:///android_asset/aos/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
        //getSupportActionBar().setTitle("AOS");
        webView = (WebView) findViewById(R.id.webView);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JavaScriptInterfacee(), "Android");
        webView.setWebChromeClient(new WebClient());
        //webView.setWebChromeClient(new WebChromeClient());
        // webView.loadUrl("file:///android_asset/www/index.html");
        //webView.loadUrl("file:///android_asset/kkk/InternetPackage.html");
        //webView.loadUrl(address+"aos.html");
        // webView.loadUrl("file:///android_asset/kkk/Fnf.html");
        // webView.loadDataWithBaseURL("file:///android_asset", html,"text/html" ,"utf-8",null);
        setWebView("aos.html");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        if(item.getItemId()==R.id.action_settings) {
//            showToast("Setting Menu Clicked..");
//            getSupportActionBar().setTitle("AOS");
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);*/
        return super.onCreateOptionsMenu(menu);
    }

    public void setWebView(final String url) {
        webView.post(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(address + url);
            }
        });

    }
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public String getContactNo() {
        contactNumber = "";
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 100);
        isFinish=false;
        while (!isFinish){

        }
        return contactNumber;
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (100):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id =
                                c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            contactNumber = phones.getString(phones.getColumnIndex("data1"));
                           // showToast(contactNumber);
                            /*String name = c.getString(c.getColumnIndex(StructuredPostal.DISPLAY_NAME));
                            Toast.makeText(this, "contact info : " + phn_no + "\n" + name, Toast.LENGTH_LONG).show();*/

                        }
                    }
                }
        }
        isFinish=true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private class JavaScriptInterfacee {
        JavaScriptInterfacee() {
        }

        @JavascriptInterface
        public void addressNavigate(String url) {
            setWebView(url);
        }

        @JavascriptInterface
        public void showToastMsg(String msg) {
            showToast(msg);
        }
        @JavascriptInterface
        public void sentSms(String sms){
            showToastMsg("SMS : " + sms);
        }
        @JavascriptInterface
        public void dialUSSD(String code){
            /*boolean b=alertBox();
            if(b){
                showToastMsg("USSD : "+code);
            }*/
            showToastMsg("USSD : "+code);

        }
        @JavascriptInterface
        public void callNumber(String number){
            showToastMsg(number);
        }
        @JavascriptInterface
        public String getContact() {
            String m = getContactNo();
            m=m.replace("+88","").replace("-","").replace(" ", "");
            showToastMsg(m);
            return m;
        }
        @JavascriptInterface
        public void getDevice(String query) {
            showToastMsg("Device : " + query);
        }
        @JavascriptInterface
        public void call(String number){
            showToastMsg("Call : "+number);
        }
        @JavascriptInterface
        public String registration(String s){
            try {
                Date date = null;
                try {
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(s);
                } catch (ParseException e) {
                }
                s=new SimpleDateFormat("dd MMM yyyy").format(date);
                return s;
            }catch (Exception e){
                return "";
            }
        }
    }

    public class WebClient extends WebChromeClient{
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {

            AlertDialog alertDialog=new AlertDialog.Builder(view.getContext()).setTitle("hello").setMessage("Do you want?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //showToast("Yes Preased...");
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //showToast("No Preased...");
                }
            }).setCancelable(true).show();

            result.confirm();
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            AlertDialog alertDialog=new AlertDialog.Builder(view.getContext()).setTitle("Dear User").setMessage("Do you want confirm this action?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //showToast("Accept Preased...");
                    result.confirm();
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //showToast("Cancel Preased...");
                    result.cancel();
                }
            }).setCancelable(true).show();


            return true;
        }
    }
}
